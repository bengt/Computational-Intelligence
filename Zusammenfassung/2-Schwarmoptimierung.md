Q: Was sind die grundlegenden Operatoren der Schwarmintelligenz?



-   Addiere Geschwindigkeit

Q: Wie lässt sich Trägheit integrieren?

Shi und Eberhart:

v' = w  v + c_1 r_1 (p^b -x) + c_2 r_2 (p^g - x)

Effekt: Bessere Konvergenz, wenn im Laufe der Zeit verringert.

Q: Wie lässt sich Schwarmintelligenz fürr diskrete Lösungsräume definieren?

Man fasst die Vertauschungen der Genelemente als Geschwindigkeit auf:

v = {(i_k, j_k) | k = 1, ... ,j}
