<!--
Buch: Kapitel 2
Skript: Kapitel 1
-->

Q:  Was sind Evolutionsstrategien?

Evolutionsstrategien sind ...

-   globale,
-   naturinspirierte,
-   robuste,
-   verbreitete,
-   universell einsetzbare,

Optimierungsverfahren.

Evolutionsstrategien

-   betrachten Problem als Blackbox.
-   finden optimale Lösungen für ohne Problemwissen.
-   stellen kaum Anforderungen an den Suchraum.
-   erkunden den Suchraum stochastisch.
-   erfolgreich in reellwertigen Lösungsräumen.

Q: Welche zentrale Anforderung stellen ES an den Suchraum?

>   Jeder Lösung muss eine Qualität zu geordnet werden können.

Q:  Was sind die drei wichtigsten Mechanismen für Evolutionsstrategien?

1.  Paarung / Rekombination (recombination)
2.  Mutation / (mutation)
3.  Selektion (selection)

Q:  Wie funktionieren Evolutionsstrategien grundsätzlich?

Im ganzen Satz:

>   Der Algorithmus verfügt über eine *Population* von Lösungen,
>   die durch Paarung entstehen und die *mutiert* werden.
>   Schließlich werden die besten Lösungen *selektiert*,
>   um in die nächste *Generation* übernommen zu werden.

Q:  Wie lässt sich Evolution zur Optimierung nutzen?

-   Die Mutation führt stochastisch zu neuen Lösungen.
-   Die Selektion bevorzugt Individuen mit hoher Fitness.

Q:  Was ist ein Optimierungsproblem?

-   Ein Minimierungsproblem.
-   Maß für Qualität: Kostenfunktion f(x), bei EA auch: Fitnessfunktion

Def. Optimierungsproblem:

>   Sei $f: X → R$ die zu minimierde Fitnessfunktion in einem beliebigen Suchraum X.
>   Finde dort dein Element $x*$, so dass $f(x*) \leq f(x) für alle x in X$

Q:  Was ist ein Beispiel für ein Optimierungsproblem?

-   Problem des Handelsreisenden (Travelling Salesman Problem, TSP)

Q:  Was sind Grundformen evolutionärer Verfahren?

-   Genetische Algorithmen
-   Evolutionsstrategien
-   evolutionäre Programmierung

Q:  Was sind Genetische Algorithmen?

-   John Holland (Anfang 1970): Adaptation in Natural and Artificial Systems
-   Ziel: Adaptives Verhalten erzeugen
-   Individuen: Binäre Strings aus Segmenten
-   Variantionsoperator: Rekombination
-   Selektion: Fitnessproportional

Q:  Was ist eine Evolutionsstrategie?

-   Rechenberg und Schefel (1960, TU Berlin)
-   Haupteinsatzgebiet
    -   anfangs: diskrete Optimierung
    -   heute: numerische Optimierung (in Reellen Zahlen)
-   Individuum: Vektor reeller Zahlen und Menge von Strategieparametern (\sigma)
-   Variantionsoperatoren:
    -   Gauß-Mutation (vor allem in reellen Zahlen)
    -   intermediäre Rekombination
    -   dominante Rekombination

Q: Was ist evolutionäre Programmierung?

-   Fogel, Owens und Walsh (19??)
-   Suchraum: hörere Abstaktionsebene, keine Zahlenvektoren
-   Ursprüngliches Ziel:
    -   kleine
    -   deterministische
    -   endliche Automaten
    -   mit großer Verallgemeinerungsfähigkeit erzeugen
-   Methode:
    -   System lernt Beispiele (Eingabewörter + korrekte Ausgabe)
    -   System ahmt Verhalten nach
-   Variantionsoperator (nach erweiterung auf reellwertige Zahlen):
    -   normalverteilte Mutation
    -   selbstadaptativer Mutationsanpassungsmechnanismus
-   Selektion:
    -   stochastische Turnierselektion
    -   50 % der Eltern, 50 % der Nachkommen
-   Rekombination: bis heute verpönt

Q:  Was ist genetische Programmierung?

-   John Koza (Ende der 1980er)
-   Idee: Computerprogramme automatisch generieren
-   Ansatz:
    -   Programme aus Elementen der imperativen und prozeduralen Progammierung

        Rekursion, Schleifen, Subroutinen
    -   zur Aufgabenlösung evolvieren
-   Individuum: Programm
    -   Repräsentation: Bäume, Assembler, LISP
-   Fitness: Qualität der Ausgabe
-   Beispiel: Steuerungsprogramm für Laufroboter

Q: Was ist die Mutation?

-   Aufgabe: Exploration des Suchraumes
-   Hauptquelle für genetische Variation
-   Anforderungen:
    -   mit hoher Wahrscheinlichkeit eher kleine Änderungen an der Lösung erzeugen
    -   Ausgehend von einem beliebigen aber festen Punkt im Suchraum muss jeder andere Punkt im Suchraum erreichbar sein.

        Ansonsten könnte eine optimale Lösung evtl. nicht gefunden werden.
    -   Die genetische Variantion sollte keinen Drift aufweisen,
        sondern sich in alle Richtungen des Suchraums mit gleicher Wahrscheinlichkeit bewegen.

        Denn die Richtung in der das Optimum liegt ist unbekannt.
        Erst die Selektion drängt durch die Fitnesswerte der Nachkommen den Suchprozess in eine bestimmte Richtung.
    -   Stärke der Mutation (Mutationsrate) soll einstellbar sein.

        Um erfolgreiche Schritte in der Fitnesslandschaft zu ermöglichen.

Q: Was sind Beispiele für Mutationsoperatoren?

-   TODO

Q: Was ist Selektion?

-   Aufgabe: Durch Mutation gewonnene Information der aktuellen Generation ausnutzen.

Q: Welche grundlegenden Arten von Selektion gibt es?

-   "+"-Selektion: Fitteste Individuen aus der Menge Eltern *und* Kinder.
-   ","-Selektion: Fitteste Kinder. Alle Eltern sterben.

Q: Wie nennt man die unausgeprägte Eigenschaft eines Individuums auch?

Gen.

Q: Was ist Rekombination/crossover?

-   Idea: combine genes of good solutions (parents) into new solutions (offspring)

Q: Was ist die "building block hypothesis"?

-   Goldberg: good substrings propagate over generations

Q: Was ist der "genetic repair effect"?

-   by Beyer: inherit the substrings the parents have in common

## Runtime Analysis

Q: Was ist die Methode der fitness-based partitions?

Unterteile den Wertebereich der Fitness-Funktion arbiträr (?).

Betrachte die Wahrscheinlichkeiten mit der eine Lösung einer Partition eine Lösung mutiert wird, die in einer anderen Partition liegt.

Die Aufstiegsahrscheinlichkeit für Bernoulli Versuche ist der Kehrwert der Aufstiegswahrscheinlichkeit.

Für OneMax ist die obere Grenze für die Laufzeit die Summe der Aufstiegzeiten der Gen-Elemente.



Ergebnis: (1 + 1)-EA on OneMax hat maximale Laufzeit von O(N log N)
