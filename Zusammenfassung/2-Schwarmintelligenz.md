Q: Welche Grundprinzipien zeigen Partikelschwärme?

-   Stigmergie: Individuen kommunizieren mittels der Umwelt.
-   Emergenz: Schwarm verhält sich intelligenter als seine Individuen.

Q: Welche Kräfte wirken grundsätzlich auf Partikel ein?

-   Zusammenhalt: Partikel orientieren sich an der Position ihrer Nachbarn.
-   Ausrichtung: Partikel bewegen sich in ähnliche Richtung wie ihre Nachbarn.
-   Trennung: Partikel vermeiden Kollisionen mit ihren Nachbarn.

Q: Wie modellieren Lee Spector et al. künstliches Leben?

Ein Partikel passt seine Geschwindigkeit anhand der relativen Geschwindigkeit zu interessanten Punkten an:

Zu diesen zählen:

-   schwerpunkt seiner spezies
-   Schwerpunkt aller Agenten
-   mittelpunkt der Umwelt
-   energiequellen
-   druchschnittsrichtung der Nachbarn

Evolution: Anpassung der Vorfaktoren / Gewichte für die Einflüsse auf den Partikel.

Q: Wie bewegt sich ein Partikel im Raum?

Seine neuen Position ergibt sich aus seiner alten Position und seiner aktuellen Geschwindigkeit:

>   x'_i = x_i + v_i

Geschwindigkeit analog:

>   v' = v + c_1 r_1 (p^b - x) + c_2 r_2 (p^g - x)

wobei

-   p^b: Beste selbst erreichte Position
-   p^g: Beste von allen / Nachbarn erreichte Position

Q: Was sind die grundlegenden Operatoren der Schwarmintelligenz?

-   Berechne p^s und p^g
-   Geschwindigkeit v anpassen
-   Partikelposition x anpassen
-   Berechne fitness(x)

Q: Wie integrieren Shi und Eberhart Trägheit?

>   v' = w  v + c_1 r_1 (p^b - x) + c_2 r_2 (p^g - x)

Effekt: w im Laufe der Zeit verringern führt zu besserer Konvergenz.

Q: Wie lässt sich Schwarmintelligenz fürr diskrete Lösungsräume definieren?

Man fasst die Vertauschungen der Genelemente als Geschwindigkeit auf:

>   v = {(i_k, j_k) | k = 1, ... ,j}
