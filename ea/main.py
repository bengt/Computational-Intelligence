from ea.models.population import IntListPopulation, Population
from ea.models.world import World
from ea.operators.adaptation import rechenberg
from ea.operators.crossover import crossover_dominant as crossover
from ea.operators.fitness import onemax_int as fitness
from ea.operators.mutation import mutate_gaussian as mutate
from ea.operators.selection import select_fittest as select

# from ea.operators.adaptation import deterministic
# from ea.operators.adaptation import selfadaptation
# from ea.operators.crossover import crossover_mode as crossover
# from ea.operators.crossover import crossover_1_point as crossover
# from ea.operators.crossover import crossover_n_point as crossover
# from ea.operators.crossover import crossover_intermediate as crossover2
# from ea.operators.fitness import onemin as fitness
# from ea.operators.fitness import onemax as fitness
# from ea.operators.fitness import onemax_int as fitness
# from ea.operators.fitness import sphere as fitness
# from ea.operators.mutation import mutate_flip as mutate
# from ea.operators.mutation import mutate_reset as mutate
# from ea.operators.mutation import mutate_random_reset as mutate

# Configuration
GENE_SIZE = 1  # size of the individuals' in genes
MU = 2  # size of the population in individuals
LAMBDA = 1  # maximum number of children
RHO = 2  # number of parents. Usually 2, more possible.

SIGMA = 10  # standard deviation of the gaussian distribution used for mutation
#             aka sigma aka mutation strength aka step size
A_RECHENBERG = 0.5  # < 1. Modification factor for 1/5 Rechenberg success rule

MAX_GENERATIONS = 100000  # limit for number of generations
MIN_FITNESS = 0  # limit for best fitness

MODE = ['+', ','][0]  # 0/+ for individuals living one generation, 1/ otherwise


def ea(SIGMA=SIGMA):
    # initialize
    world = World(fitness=fitness)
    world.population = IntListPopulation(length=MU, gene_size=GENE_SIZE)

    # iterate over generations
    for generation_count in range(MAX_GENERATIONS):

        # log this generation's population to stdout
        print("%s: %s" % (generation_count, world.population))

        # if we reached a sufficiently good solution, stop
        if min(world.scores) < MIN_FITNESS:
            break

        # select rho fittest individuals as parents
        parents = select(world=world, rho=RHO)

        # generate children by crossing over the parents
        children_original = []
        while len(children_original) < LAMBDA:
            children_original += crossover(population=parents)

        # mutate children
        children = [mutate(individual=individual, mu=0, sigma=SIGMA)
                    for individual in children_original]

        # self-adapt mutation strenght using Rechenberg's success rule
        SIGMA = rechenberg(children_original=children_original,
                           children_mutated=children, fitness=world.fitness,
                           a=A_RECHENBERG, sigma=SIGMA)

        if MODE == '+':
            # extend population with children
            world.population += Population.from_individuals(
                individuals=children)
        elif MODE == ',':
            # replace population with children
            world.population = Population.from_individuals(
                individuals=children)

        # select fittest mu individuals for next generation
        world.population = select(rho=MU, world=world)

    # log best individual
    print("Best individual is %s with a score of %s." %
          (select(world=world, rho=1)[0], min(world.scores)))

if __name__ == '__main__':
    ea()
