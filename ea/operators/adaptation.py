import math
import random


def rechenberg(children_original, children_mutated, fitness, a, sigma):
    """
    Increases the standard deviation, if more than 1/5 of mutations are
    successful and decreases it if less then 1/5 are.
    """

    # count successful mutations
    successes = [1 for (old, new) in zip(children_original, children_mutated)
                 if fitness(old) > fitness(new)]
    if len(successes) / len(children_mutated) < 1/5:
        print("Decreasing mutation strength.")
        sigma = sigma / a
    elif len(successes) / len(children_mutated) > 1/5:
        print("Increasing mutation strength.")
        sigma = sigma * a

    return sigma


def deterministic(t, T):

    # T Max generations
    # t current generation
    sigma = 1 - 0.9 * (t / T)

    return sigma


def selfadaptation(tau, sigma):

    sigma = sigma * math.e ** (tau * random.gauss(0, 1))

    return sigma


def test():
    pass  # TODO add tests
