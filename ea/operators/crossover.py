import random

import statistics
from ea.models.individual import Individual
from ea.models.population import (BoolListPopulation, ChrListPopulation,
                                  IntListPopulation, Population)


# random.seed(42)  # enable for persistent results, disable for random results


def crossover_mode(population):
    """
    The children's gene will have the bases most common amoung its parents.
    """

    if type(population) == BoolListPopulation:
        return crossover_mode_bool(population)


def crossover_mode_bool(population):
    gene = []

    for i in range(len(population[0].gene)):
        base = sum([1 for parent in population if parent.gene[i-1]]) > \
            len(population) / 2
        gene.append(base)

    individual = Individual(gene=gene)

    return Population.from_individuals(individuals=[individual])


def crossover_1_point(population, n=1):
    return crossover_n_point(population=population, n=n)


def crossover_n_point(population, n=1):
    assert isinstance(population, Population)

    # select crossover point
    valid_positions = range(1, population.gene_size-1)
    points = [random.choice(valid_positions) for _ in range(n)]

    # generate children from parents
    children = []

    # iterate over children
    for i in range(len(population)):
        gene_child = []

        # iterate over crossover points
        for i in range(-1, n):
            if i == -1:
                # part left of the first crossover point
                gene_child += population[0].gene[0:points[0]]
                continue

            key = i % population.gene_size - 1
            parent = population[key]

            try:
                gene_child += parent.gene[points[i]:points[i+1]]
            except IndexError:
                # this is the last point
                gene_child += parent.gene[points[i]:]

        child_individual = Individual(gene=gene_child)
        children.append(child_individual)

    return Population.from_individuals(children)


def crossover_dominant(population):
    """
    Collects child gene from equivalent gene elements of a random parent.
    """
    assert isinstance(population, Population)

    gene = []
    for gene_index in range(population.gene_size):
        parent_index = random.choice(range(len(population)))
        gene.append(population[parent_index].gene[gene_index])

    return Population.from_individuals(individuals=[Individual(gene=gene)])


def crossover_intermediate(population, alpha=None):
    """
    Generated children feature averages of the equivalent parent gene elements.
    """
    assert isinstance(population, Population)
    assert isinstance(population.individuals[0].gene[0], (bool, int, float))
    assert len(population) == 2, "this works with two parents, only."
    # TODO: How about a universal formula (for p > 2)?

    gene = []
    for gene_index in range(population.gene_size):
        features_parents = [parent.gene[gene_index]
                            for parent in population.individuals]

        if alpha is None:
            # calculate average feature
            feature = statistics.mean(features_parents)

            # cast back to original type
            feature = type(population.individuals[0].gene[0])(feature)
        else:
            # reweight according to alpha
            feature = sum([alpha * features_parents[0],
                           (1 - alpha) * features_parents[1]])

        print(feature)
        gene.append(feature)

    return Population.from_individuals(individuals=[Individual(gene=gene)])


def test():
    parents_bool = BoolListPopulation(length=0)
    parents_bool.append(Individual([True]))
    parents_bool.append(Individual([True]))
    children = crossover_mode(population=parents_bool)
    assert type(children[0].gene) == tuple
    assert type(children[0].gene[0]) == bool
    assert children[0].gene

    parents_chr = ChrListPopulation()
    parents_chr.append(Individual(gene=['a', 'b', 'c', 'd', 'e']))
    parents_chr.append(Individual(gene=['e', 'd', 'c', 'b', 'a']))
    children_chr = crossover_1_point(population=parents_chr)

    parents_chr = ChrListPopulation()
    parents_chr.append(Individual(gene=['a', 'b', 'c']))
    parents_chr.append(Individual(gene=['c', 'b', 'a']))
    children_chr = crossover_1_point(population=parents_chr)
    assert len(children_chr) == 2

    parents_chr = ChrListPopulation()
    parents_chr.append(Individual(gene=['a', 'b', 'c']))
    parents_chr.append(Individual(gene=['c', 'b', 'a']))
    children_chr = crossover_n_point(population=parents_chr, n=3)
    assert len(children_chr) == 2

    parents_chr = ChrListPopulation()
    parents_chr.append(Individual(gene=['a', 'b', 'c']))
    parents_chr.append(Individual(gene=['c', 'b', 'a']))
    children_chr = crossover_dominant(population=parents_chr)
    assert len(children_chr) == 1

    parents_int = IntListPopulation()
    parents_int.append(Individual(gene=[1]))
    parents_int.append(Individual(gene=[3]))
    children_int = crossover_intermediate(population=parents_int)
    assert len(children_int) == 1
    assert children_int[0].gene[0] == 2
