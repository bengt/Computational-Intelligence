from ea.models.population import BoolListPopulation, IntListPopulation
from ea.models.world import World
from ea.operators.fitness import onemin_int as fitness


def select_fittest(world, rho):
    X = world.scores
    Y = world.population.individuals
    parents = [x for (_, x) in sorted(zip(X, Y))][:rho]

    if isinstance(parents[0].gene[0], bool):
        population = BoolListPopulation(length=0)
    elif isinstance(parents[0].gene[0], int):
        population = IntListPopulation(length=0)

    for parent in parents:
        population.append(parent)

    return population


def test():
    WORLD = World(fitness=fitness)
    WORLD.population = IntListPopulation(length=10)
    rho = 2
    PARENTS = select_fittest(WORLD, rho=rho)
    assert len(PARENTS) == rho
    assert PARENTS[0].gene[0] == 0
    assert PARENTS[1].gene[0] == 0
