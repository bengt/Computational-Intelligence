import sys

from ea.models.individual import Individual


def onemin_int(individual):
    assert isinstance(individual, Individual)

    assert type(individual.gene[0]) == int, \
        "Individual's type must be int, not %s." % type(individual.gene[0])

    return sum([int(integer) for integer in individual.gene])


def onemax_int(individual):
    assert isinstance(individual, Individual)

    assert type(individual.gene[0]) == int, \
        "Individual's type must be int, not %s." % type(individual.gene[0])

    return sum([sys.maxsize - int(integer) for integer in individual.gene])


def onemin(individual):
    """
    Counts the Falses in the gene of a given individual.
    """
    assert isinstance(individual, Individual)

    assert type(individual.gene[0]) == bool, \
        "Individual's type must be bool, not %s." % type(individual.gene[0])

    return sum([1 - int(boolean) for boolean in individual.gene])


def onemax(individual):
    """
    Counts the Trues in the gene of a given individual.
    """
    assert isinstance(individual, Individual)

    assert type(individual.gene[0]) == bool, \
        "Individual's type must be bool, not %s." % type(individual.gene[0])

    return sum([int(boolean) for boolean in individual.gene])


def sphere(individual):
    assert isinstance(individual, Individual)
    assert type(individual.gene[0]) == float, \
        "Individual's type must be float, not %s." % type(individual.gene[0])

    return sum([x**2 for x in individual.gene])


def test():
    gene = [0]
    individual_int = Individual(gene=gene)
    assert onemin_int(individual_int) == 0

    gene = [10]
    individual_int = Individual(gene=gene)
    assert onemax_int(individual_int) == sys.maxsize - 10

    gene = [True]
    individual_bool = Individual(gene=gene)
    assert onemin(individual_bool) == 0

    gene = [True]
    individual_bool = Individual(gene=gene)
    assert onemax(individual_bool) == 1
