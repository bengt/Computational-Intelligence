from __future__ import division

import random
import unittest

from ea.models.enumeration import PrimaryColor
from ea.models.individual import Individual


def mutate_flip(individual):
    assert isinstance(individual, Individual)

    assert type(individual.gene[0]) == bool, \
        "Individual's gene bases must be bool."

    return mutate_flip_bool(individual)


def mutate_flip_bool(individual):
    assert isinstance(individual, Individual)

    gene = list(individual.gene)  # mutable copy
    for i, bit in enumerate(individual.gene):
        if random.random() > 1/len(individual.gene):
            gene[i] = not bit  # flip bit

    return Individual(gene=gene)  # new individual


def mutate_reset_enum(individual):
    assert isinstance(individual, Individual)

    gene = list(individual.gene)  # mutable copy
    colors = [element for element in individual.gene[0].__class__]
    for i in range(len(individual.gene)):
        if random.random() > 0.5:
            gene[i] = random.choice(colors)
    individual = Individual(gene=gene)
    return individual


def random_reset_mutate(individual):
    assert isinstance(individual, Individual)

    for i in range(len(individual.gene)):
        if random.random() > 1/len(individual.gene):
            individual.gene[i] = random.choice(individual.enum)
    return individual


def mutate_gaussian(individual, mu, sigma):
    """
    Chooses the offspring's gene elements from a gauss variate.
    """
    assert isinstance(individual, Individual)

    gene = []
    for feature in individual.gene:
        value = random.gauss(mu=mu, sigma=sigma)
        # round and cast value to the individual's original type
        if type(individual.gene[0]) == int:
            value = round(value)
        value = type(individual.gene[0])(value)
        value += feature
        gene.append(value)

    return Individual(gene=gene)


def test():

    gene = [False]
    individual = Individual(gene=gene)
    individual = mutate_flip(individual)
    assert isinstance(individual.gene, tuple)
    assert isinstance(individual.gene[0], bool)

    individual = Individual([False, True])
    individual = mutate_flip(individual)
    assert len(individual.gene) == 2
    for gene_bit in individual.gene:
        assert isinstance(gene_bit, bool)

    individual = Individual([PrimaryColor.blue, PrimaryColor.red])
    individual = mutate_reset_enum(individual)
    assert len(individual.gene) == 2
    for gene_bit in individual.gene:
        assert isinstance(gene_bit, individual.atomtype)

    individual_int = Individual([0, 1, 2])
    individual_int = mutate_gaussian(individual_int, mu=0, sigma=1)
    assert len(individual_int.gene) == 3
    for element in individual_int.gene:
        assert isinstance(element, individual_int.atomtype)


class MyTestCase(unittest.TestCase):
    def test1(self):
        gene = [1]
        individual = Individual(gene=gene)
        self.assertRaises(Exception, mutate_flip, individual=individual)
