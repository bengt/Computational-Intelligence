import unittest


class Interval(object):
    def __init__(self, minimum, maximum):
        if minimum > maximum:
            raise Exception("lower bound must be smaller than the upper.")
        if type(minimum) is not type(maximum):
            raise Exception("lower and upper bound must have the same type.")
        self._minimum = minimum
        self._maximum = maximum

    @property
    def minimum(self):
        return self._minimum

    @property
    def maximum(self):
        return self._maximum


def test():
    minimum = 0
    maximum = 10
    interval = Interval(minimum=minimum, maximum=maximum)
    assert interval.minimum == minimum
    assert interval.maximum == maximum


class MyTestCase(unittest.TestCase):
    def test1(self):
        minimum = 1
        maximum = 0

        self.assertRaises(Exception, Interval, minimum=minimum,
                          maximum=maximum)

    def test2(self):
        minimum = 0
        maximum = 1.0

        self.assertRaises(Exception, Interval, minimum=minimum,
                          maximum=maximum)
