#! /usr/bin/env python3

import unittest

from enum import Enum

from ea.models.enumeration import PrimaryColor


class Individual(object):

    def __init__(self, gene):
        assert type(gene) is list or type(gene) is tuple, \
            "Gene must be a list or a tuple, not %s." % \
            type(gene)

        # make gene immutable
        if type(gene) == list:
            gene = tuple(gene)

        # auto detect atom type
        atomtype = type(gene[0])

        self._check_atomtype(atomtype=atomtype)
        self._atomtype = atomtype

        self._check_gene(gene)
        self._gene = gene

    def __str__(self):
        return str(self._gene)

    def __gt__(self, other):
        assert isinstance(other, Individual)
        return self.gene > other.gene

    @property
    def gene(self):
        return self._gene

    @property
    def atomtype(self):
        return self._atomtype

    def _check_atomtype(self, atomtype):
        if not issubclass(atomtype, (bool, int, float, str, Enum)):
            raise Exception('enum needs to be a base type or an enum.')

    def _check_gene(self, gene):
        if type(gene) == tuple:
            gene = gene[0]

        if not isinstance(gene, self._atomtype):
            raise Exception('gene needs to be of type %s, not %s' %
                            (self._atomtype, type(gene)))


def test():
    gene = [True]
    individual = Individual(gene=gene)
    assert individual.gene == tuple(gene)
    assert str(individual) == str(tuple(gene))

    gene = [10]
    individual = Individual(gene=gene)
    assert individual.gene == tuple(gene)
    assert str(individual) == str(tuple(gene))

    gene = [3.1415]
    individual = Individual(gene=gene)
    assert individual.gene == tuple(gene)
    assert str(individual) == str(tuple(gene))

    gene = ['a']
    individual = Individual(gene=gene)
    assert individual.gene == tuple(gene)
    assert str(individual) == str(tuple(gene))

    gene = [PrimaryColor.green]
    individual = Individual(gene=gene)
    assert individual.gene == tuple(gene)
    assert str(individual) == str(tuple(gene))

    gene = [False, True, True]
    individual = Individual(gene=gene)
    assert individual.gene == tuple(gene)
    assert str(individual) == str(tuple(gene))

    gene = [1, 3, 0]
    individual = Individual(gene=gene)
    assert individual.gene == tuple(gene)
    assert str(individual) == str(tuple(gene))

    gene = [1.0, 3.1415, 0.0]
    individual = Individual(gene=gene)
    assert individual.gene == tuple(gene)
    assert str(individual) == str(tuple(gene))

    gene = ['a', 'c', 'd']
    individual = Individual(gene=gene)
    assert individual.gene == tuple(gene)
    assert str(individual) == str(tuple(gene))

    gene = [PrimaryColor.red, PrimaryColor.green]
    individual = Individual(gene=gene)
    assert individual.gene == tuple(gene)
    assert str(individual) == str(tuple(gene))


class MyTestCase(unittest.TestCase):

    def test1(self):

        class MyObject(object):
            def __init__(self):
                pass

        gene = [MyObject()]

        self.assertRaises(Exception, Individual, gene=gene)
