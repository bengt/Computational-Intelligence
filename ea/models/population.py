#! /usr/bin/env python3

import random
import string
from abc import ABCMeta, abstractmethod

from ea.models.individual import Individual


class Population(object):

    __metaclass__ = ABCMeta

    def __init__(self, length, gene_size):

        self._check_gene_size(gene_size=gene_size)
        self._gene_size = gene_size

        self._individuals = []
        for _ in range(length):
            self.add_default_individual()

    @property
    def gene_size(self):
        return self._gene_size

    @classmethod
    def from_individuals(self, individuals):
        assert type(individuals) == list
        assert all([isinstance(individual, Individual)
                    for individual in individuals])

        gene_size = len(individuals[0].gene)

        if type(individuals[0].gene[0]) == bool:
            population = BoolListPopulation(length=0, gene_size=gene_size)

        if type(individuals[0].gene[0]) == int:
            population = IntListPopulation(length=0, gene_size=gene_size)

        if type(individuals[0].gene[0]) == str:
            population = ChrListPopulation(length=0, gene_size=gene_size)

        for individual in individuals:
            population.append(individual)

        return population

    def add_default_individual(self):
        individual = self.get_default_individual()
        self._individuals.append(individual)

    def add_random_individual(self):
        individual = self.get_random_individual()
        self._individuals.append(individual)

    def __str__(self):
        return str([individual.gene for individual in self._individuals])

    def __getitem__(self, key):
        return self._individuals[key]

    def __len__(self):
        return len(self._individuals)

    def append(self, individual):
        if self._gene_size is None:
            self._gene_size = len(individual.gene)
        self.check_individual(individual)
        self._individuals.append(individual)

    def __add__(self, population):
        assert isinstance(population, Population)
        assert type(self) == type(population)

        if len(population) == 0:
            return self

        if len(self) == 0:
            return population

        if type(self.individuals[0].gene[0]) == bool:
            population_all = BoolListPopulation()

        elif type(self.individuals[0].gene[0]) == str:
            population_all = ChrListPopulation()

        elif type(self.individuals[0].gene[0]) == int:
            population_all = IntListPopulation()

        for individual in self._individuals + population.individuals:
            population_all.append(individual)

        return population_all

    @property
    def individuals(self):
        return self._individuals

    def _check_gene_size(self, gene_size):
        assert type(gene_size) == int and gene_size >= 0 or\
            gene_size is None, \
            "gene_size must be a positive integer or none, not %s of type %s." \
            % (gene_size, type(gene_size))

    @abstractmethod
    def get_default_individual(self):
        pass  # to be overridden by implementations

    @abstractmethod
    def get_random_individual(self):
        pass  # to be overridden by implementations

    @abstractmethod
    def check_individual(self, individual):
        pass  # to be overridden by implementations


class BoolListPopulation(Population):

    def __init__(self, length=0, gene_size=None):
        super(BoolListPopulation, self).__init__(length=length,
                                                 gene_size=gene_size)

    def get_default_individual(self):
        gene = [True for _ in range(self._gene_size)]
        return Individual(gene=gene)

    def get_random_individual(self):
        gene = [random.choice([True, False]) for _ in range(self._gene_size)]
        return Individual(gene=gene)

    def check_individual(self, individual):
        assert type(individual.gene) is tuple, \
            "Individual's gene must be a tuple, not %s." % \
            type(individual.gene)
        assert type(individual.gene[0]) is bool, \
            "Individual's gene type must be a bool, not %s." % \
            type(individual.gene[0])


class IntListPopulation(Population):

    def __init__(self, length=0, gene_size=None):
        super(IntListPopulation, self).__init__(length=length,
                                                gene_size=gene_size)

    def get_default_individual(self):
        gene = [0]
        return Individual(gene=gene)

    def get_random_individual(self):
        gene = [random.choice(range(0, 9))]
        return Individual(gene=gene)

    def check_individual(self, individual):
        assert type(individual.gene) is tuple, \
            "Individual's gene must be a tuple, not %s." % \
            type(individual.gene)
        assert type(individual.gene[0]) is int, \
            "Individual's gene type must be an int, not %s." % \
            type(individual.gene[0])


class FloatListPopulation(Population):

    def __init__(self, length=0, gene_size=None):
        super(FloatListPopulation, self).__init__(length=length,
                                                  gene_size=gene_size)

    def get_default_individual(self):
        gene = [0.0]
        return Individual(gene=gene)

    def get_random_individual(self):
        gene = [random.random()]
        return Individual(gene=gene)

    def check_individual(self, individual):
        assert type(individual.gene) is tuple, \
            "Individual's gene must be a tuple, not %s." % \
            type(individual.gene)
        assert type(individual.gene[0]) is float, \
            "Individual's gene type must be a float, not %s." % \
            type(individual.gene[0])


class ChrListPopulation(Population):

    def __init__(self, length=0, gene_size=None):
        super(ChrListPopulation, self).__init__(length=length,
                                                gene_size=gene_size)

    def get_default_individual(self):
        gene = ["a"]
        return Individual(gene=gene)

    def get_random_individual(self):
        gene = [random.choice(string.ascii_letters)]
        return Individual(gene=gene)

    def check_individual(self, individual):
        assert type(individual.gene) is tuple, \
            "Individual's gene must be a tuple, not %s." % \
            type(individual.gene)
        assert type(individual.gene[0]) is str, \
            "Individual's gene type must be a str, not %s." % \
            type(individual.gene[0])
        assert all([len(individual.gene[i]) is 1
                    for i in range(len(individual.gene))]), \
            "Gene must be single characters."


def test():
    assert Population(length=0, gene_size=1).get_random_individual() is None
    assert Population(length=0, gene_size=1).check_individual(individual=None) \
        is None

    gene_size = 1
    population = BoolListPopulation(gene_size=gene_size)
    assert population.individuals == []
    population.add_default_individual()
    assert len(population.individuals) == 1
    gene = [True]
    population.append(Individual(gene=gene))
    assert len(population.individuals) == 2
    population.add_random_individual()
    assert len(population.individuals) == 3

    population = IntListPopulation(gene_size=gene_size)
    assert population.individuals == []
    population.add_default_individual()
    assert len(population.individuals) == 1
    gene = [1]
    population.append(Individual(gene=gene))
    assert len(population.individuals) == 2
    population.add_random_individual()
    assert len(population.individuals) == 3

    population = FloatListPopulation(gene_size=gene_size)
    assert population.individuals == []
    population.add_default_individual()
    assert len(population.individuals) == 1
    gene = [3.1415]
    population.append(Individual(gene=gene))
    assert len(population.individuals) == 2
    population.add_random_individual()
    assert len(population.individuals) == 3

    population = ChrListPopulation(gene_size=gene_size)
    assert population.individuals == []
    population.add_default_individual()
    assert len(population.individuals) == 1
    gene = ["b"]
    population.append(Individual(gene=gene))
    assert len(population.individuals) == 2
    population.add_random_individual()
    assert len(population.individuals) == 3

    population2 = ChrListPopulation()
    population2.add_default_individual()
    population2 += population
    assert len(population2) == len(population) + 1

    population2 = ChrListPopulation()
    population2 += population
    assert len(population2) == len(population)
