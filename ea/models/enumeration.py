from enum import Enum


class PrimaryColor(Enum):
    red = 1
    green = 2
    blue = 3


class Nucleotide(Enum):
    A = 1
    G = 2
    T = 3
    C = 4


class Numeral(Enum):
    zero = 0
    one = 1
    two = 2
    three = 3
    four = 4
    five = 5
    six = 6
    seven = 7
    eight = 8
    nine = 9


def test():
    for color in PrimaryColor:
        assert color.value > 0

    for nucleotide in Nucleotide:
        assert nucleotide.value > 0

    for numeral in Numeral:
        assert numeral.value < 10
