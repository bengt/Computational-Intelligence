from ea.models.population import IntListPopulation, Population
from ea.operators.fitness import onemin_int


class World(object):

    def __init__(self, fitness):
        self._population = None
        self._fitness = fitness

    @property
    def fitness(self):
        return self._fitness

    @property
    def scores(self):
        return [self._fitness(individual) for individual in
                self.population.individuals]

    @property
    def population(self):
        return self._population

    @population.setter
    def population(self, population):
        assert isinstance(population, Population)
        self._population = population


def test():
    world = World(fitness=onemin_int)
    world.population = IntListPopulation(length=10, gene_size=0)
    assert len(world.population.individuals) == 10
    print(world.scores)

    world.population.append(world.population.get_random_individual())
    world.population.append(world.population.get_random_individual())
    world.population.append(world.population.get_random_individual())
    assert len(world.population.individuals) == 13
    print(world.scores)
